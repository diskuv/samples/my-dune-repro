module M = struct
  let main () =
    prerr_endline
      "\n\
       Problem: In the module src/DkHelloScript_Std/N0xxLanguage/Example051.ml\n\
      \         there is no `let main _ctx = ...` function." ;
    exit 3
  [@@warning "-32"]

  include DkHelloScript_Std__N0xxLanguage__Example051
end

let () = M.main ()
