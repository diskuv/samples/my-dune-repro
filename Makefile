all:
	@echo "Choose either 'opam exec -- make actual-failure' or 'opam exec -- make expected-success'"
	@echo "Any opam switch should be fine as long as it has Dune and ocamlc"

.PHONY: do-50
.PHONY: do-51
.PHONY: expected-success
.PHONY: actual-failure

SH_REPRODUCE=env -u OCAMLLIB
DUNE_REPRODUCE=dune build --sandbox=none --error-reporting=deterministic --display-separate-messages --no-buffer --no-print-directory --no-config --display=verbose @all-packages

expected-success:
	rm -rf _build
	$(SH_REPRODUCE) $(MAKE) do-50
	echo "Removing a file that should not be needed"
	rm -f _build/default/#s/DkHelloScript_Std/N0xxLanguage/.DkHelloScript_Std__N0xxLanguage__Example050.objs/byte/dkHelloScript_Std__N0xxLanguage__Example050.cmi
	$(SH_REPRODUCE) $(MAKE) do-51

actual-failure:
	rm -rf _build
	$(SH_REPRODUCE) $(MAKE) do-50
	$(SH_REPRODUCE) $(MAKE) do-51

do-50:
	rm -rf #s dune-project
	cp -rp run50/#s .
	cp -p run50/dune-project-orig dune-project
	$(DUNE_REPRODUCE)

do-51:	
	rm -rf #s dune-project
	cp -p run51/dune-project-orig dune-project
	cp -rp run51/#s .
	$(DUNE_REPRODUCE)
